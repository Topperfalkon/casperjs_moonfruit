// Only create instance in single-file mode
var casper = casper || require('casper').create({
	/* //Logging
	verbose: true,
	logLevel: "error"*/
});

casper.then(function(){
	this.test.assertEquals(this.getCurrentUrl(), url + 'billing', 'Should be on billing page');
	this.test.assertExists('a.edit_profile', 'Edit Details button should exist');
	this.test.assertSelectorHasText('a.edit_profile span.title', 'Edit details', 'Edit Details button should say "Edit details"');
	this.thenClick('a.edit_profile', function() {
		this.test.assertVisible('#confirmation_popup div.box', 'The "Save Changes" box should be visible');
		this.test.assertEvalEquals(function() {
			return document.getElementById("confirmation_popup").getElementsByTagName("input")[0].getAttribute("value");
		}, email, 'Email address should be same as originally set');
		
		// Clear the Email field, then check it's actually been cleared
		this.evaluate(function () {
			document.getElementById("confirmation_popup").getElementsByTagName("input")[0].value = "";
		});
		this.test.assertEvalEquals(function() {
			return document.getElementById("confirmation_popup").getElementsByTagName("input")[0].value;
		}, '', 'Email field should now be empty');

		//TODO: Change the email address and set it, then re-open and check again.
		//this.sendKeys('#confirmation_popup .email input', newEmail);


		

		this.test.assertEvalEquals(function() {
			return document.getElementById("confirmation_popup").getElementsByTagName("input")[0].value;
		}, newEmail, 'Email field should now contain the new email address');
	});
});

casper.run(function(){
	this.test.done();
	this.test.renderResults(false, 0);
}); 