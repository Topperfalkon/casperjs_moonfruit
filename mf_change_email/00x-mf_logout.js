// Only create instance in single-file mode
var casper = casper || require('casper').create({
	/* //Logging
	verbose: true,
	logLevel: "error"*/
});

//Test Variables
//var url = 'http://moonfruit.harley.dev/';
var loggedInUrl =  url + 'home';

//Test that user is logged in
casper.start(loggedInUrl, function(){
	if (!this.test.assertUrlMatch(/\/home/, "You should now be at ./home")){
		this.test.error()
	}
});

casper.then(function(){
	this.test.assertExists("a.logout_button span", "The Account menu should exist");
	this.test.comment("Clicking on Account to display the menu")
	this.click("a.logout_button span");
	this.test.assertVisible("div.logged_in ul.menu_dropdown", "The account menu should be displayed");
	this.test.assertVisible("#menu_option_6", "Specifically, the logout option should exist and be visible.")
});

casper.then(function(){
	this.click("#menu_option_6");
	this.waitFor(function check() {
        return (/http:\/\/www.moonfruit.com/).test(this.getCurrentUrl());
    }, function doTest() {
        this.test.assertEquals(this.getCurrentUrl(), url, "You should now be back at " + url);
    }, function onTimeout(){
    	this.test.fail("logout failed, ending the test");
    });
});

casper.run(function(){
	this.test.done();
	this.test.renderResults(true, 0);
});