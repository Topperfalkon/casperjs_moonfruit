// Only create instance in single-file mode
var casper = casper || require('casper').create({
	/* //Logging
	verbose: true,
	logLevel: "error"*/
});

casper.then(function(){
	this.test.assertEquals(this.getCurrentUrl(), url + 'home', "Should still be at home");
	this.test.assertVisible('div.logged_in span.button_title', "The account button should be visible");
	this.test.assertSelectorHasText('div.logged_in span.button_title','Account','The account button shoudl actually say "Account"');
	this.thenClick('div.logged_in a.logout_button', function(){
		this.test.assertVisible('div.logged_in ul.menu_dropdown', 'Clicking the Account button Should make the account menu visible');
		this.test.assertExists('#menu_option_3', 'The Billing option should be in the menu');
		this.test.assertSelectorHasText('#menu_option_3', 'Billing', 'The Billing option should actually say "Billing"');
	});
	this.thenClick('#menu_option_3', function(){
		this.test.assertEquals(this.getCurrentUrl(), url + "billing", 'Should now be on the billing page');
	})
});

casper.run(function(){
	this.test.done();
	this.test.renderResults(false, 0);
}); 