CasperJS Moonfruit.com
----------------------
This project is for automated testing of Moonfruit.com

Documentation by Harley Faggetter on behalf of the Moonfruit QA Team

Installation
------------
Install PhantomJS and CasperJS on your selected platform
Clone this repo to your test directory
Run `casperjs test --includes=casper-config.js [--ignore-ssl-errors] <path to test suite>`

Testing from VMs
----------------
Make sure to include `--ignore-ssl-errors=yes` after the casperjs test command