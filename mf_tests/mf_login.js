// Only create instance in single-file mode
var casper = casper || require('casper').create({
	/* // Logging
	verbose: true,
	logLevel: "error"*/
});

// Test variables
//var url = 'http://moonfruit.harley.dev';
var username = 'casperjs';
var password = 'friendlyghost';
var testUsername;

// Sanity Check - Test that some page elements exist
casper.start(url, function(){
	this.test.assertTitle('Free Website Builder - Moonfruit - Total website design control', 
		'The page title should be "Free Website Builder - Moonfruit - Total website design control"');
	this.test.assertExists('a.login_button', 'The login button must exist');
});

//Click the Login button
casper.then(function(){
	this.thenClick('a.login_button', function(){
		this.echo("Clicked the login button");
	});
});

// Test that the login popup actually appears 
casper.then(function(){
	this.test.assertVisible('#login_popup', 'Login pop-up should be visible');
});

// Enter Login info, then select Login
casper.then(function(){
	this.test.assertExists('#login_popup input[type="text"]', 'Username field should exist');
	this.test.assertExists('#login_popup input[type="password"]', 'Password field should exist');
	this.sendKeys('#login_popup input[type="text"]', username);
	this.sendKeys('#login_popup input[type="password"]', password);
	this.test.comment("Entered Username and Password, logging in");
	this.thenClick('#login_popup p.submit a', function(){
		this.test.comment("Clicked the login button");
    });
    // Wait for the page to reload, then test login was successful. Fail gracefully if login times out (or fails for other reasons)
    this.waitFor(function check() {
        return (/\/home/).test(this.getCurrentUrl());
    }, function doTest() {
        this.test.assertUrlMatch(/\/home/, "You should now be at ./home");
        this.test.assertVisible('div.logged_in', 'You should now be logged in');
        testUsername = this.fetchText('#header_items p.username');
        this.test.assertEquals(testUsername, username, 'The username of the logged in user should match the username provided');
    }, function onTimeout(){
    	this.test.error("Login failed, ending the test");
    	this.die("Login failed, terminating test now", 504);
    });
});

//End the test - Multi-file
//casper.test.done(8);


// End the test - Standalone
casper.run(function(){
	this.test.done(8);
	this.test.renderResults(false, 0);
});